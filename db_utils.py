# -*- coding: utf-8 -*-
"""
Basic sqlalchemy functionalities.
Utilities for working with sqlalchemy, mostly printing type of objects and statement results.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

from sqlalchemy import Table, create_engine
from sqlalchemy.exc import NoSuchTableError

class ReflectionFactory(object):
    """Factory for creating reflections of table in database"""
    def __init__(self, engine, metadata):
        self._engine = engine
        self._metadata = metadata

    def create_reflect(self, table_name):
        """Function for creating reflections of the tables in database"""
        try:
            return Table(table_name, self._metadata, autoload=True, autoload_with=self._engine)
        except NoSuchTableError:
            print('No such table in db: {}.'.format(table_name))
            return None

def print_type(object, desc_str=''):
    """Helper for printing object type. Desc string optional.
    """
    print('{}\t{}'.format(desc_str, type(object)))

def print_sql(object):
    """Helper for printing sql statement from sqlalchemy object"""
    print(str(object))