# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Simple data acess layer for chinook database.
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

from sqlalchemy import (create_engine, MetaData, Table)

class DataAcessLayer(object):
    """Simple data acess layer for chinook database.
    """
    con = None
    engine = None
    con_string = None
    meta = MetaData()
    artist = None
    album = None

    def __init__(self, con_string='sqlite:///chinook\Chinook_Sqlite.sqlite'):
        self.con_string = con_string
        self.engine = create_engine(con_string)
        self.con = self.engine.connect()
        self.artist = Table('Artist', meta, autoload=True, autoload_with=engine)
        self.album = Table('Album', meta, autoload=True, autoload_with=engine)

dal = DataAcessLayer()

# Using DataAccessLayer for accessing database
from sqlalchemy import select, and_, or_

artist = dal.artist
album = dal.album

sel = select([artist.c.Name, album.c.Title])
sel = sel.select_from(artist.join(album))
sel = sel.where(or_(artist.c.Name == 'Aerosmith', artist.c.Name == 'Accept'))
sel = sel.limit(10)
rp = dal.con.execute(sel)
rp.fetchall()

