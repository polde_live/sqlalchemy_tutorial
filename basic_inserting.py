# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Inserting data to local database.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

# DB should be created - if not run basic_ddl.create_cookies_database

from sqlalchemy import (MetaData, Table, Column, Integer, Numeric, String,
                        DateTime, ForeignKey, Boolean, create_engine)
from sqlalchemy import insert
from db_utils import ReflectionFactory, print_type


# Create connection to database
metadata = MetaData()
engine = create_engine(r'sqlite:///cookies.db')

reflection_factory = ReflectionFactory(engine, metadata)
print('database table names:', engine.table_names())


# Creating reflection for tables in database
cookies = reflection_factory.create_reflect('cookies')

print_type(cookies, 'type of cookies object:')


# Inserting data - table method
# Create insert statement with parameters and values

# Connect to db, display type of connection object
connection = engine.connect()
print_type(connection, 'type of connection object:')

# Check connection state
print('connection closed:', connection.closed)

# Inserting with table.values() method
ins = cookies.insert().values(
    cookie_name="chocolate chip",
    cookie_recipe_url="http://some.aweso.me/cookie/recipe.html",
    cookie_sku="CC01",
    quantity="12",
    unit_cost="0.50"
)

# Print insert object type and properties
print_type(ins, 'type of ins object')
print('ins insert statemen:t\t', str(ins))
print('ins compiled params:\t', ins.compile().params)

# Execute created insert statement 
# Result is ResultProxy object
result = connection.execute(ins)

# Show number of affected rows
def print_inserted_info(result):
    """Helper for printing info on last insert. Parameter is ResultProxy object.
    """
    result_rc = result.rowcount

    print('rows inserted:\t{}'.format(result_rc))
    # ResultProxy support inserted_primary_key only if one row is inserted
    if result_rc == 1:
        print('inserted primary key:\t{}'.format(result.inserted_primary_key))
    elif result_rc == 0:
        print('no row inserted')
    else:
        print('multiple rows inserted, no inserted_primary_key atrubyte')

print_inserted_info(result)

# Inserting by passing parameters to insert object
ins = cookies.insert()

result = connection.execute(ins, cookie_name='dark chocolate chip',
                            cookie_recipe_url='http://some.aweso.me/cookie/recipe_dark.html',
                            cookie_sku='CC02',
                            quantity='1',
                            unit_cost='0.75')

print_inserted_info(result)

# Inserting python list of values
inventory_list = [
    {
        'cookie_name': 'peanut butter',
        'cookie_recipe_url': 'http://some.aweso.me/cookie/peanut.html',
        'cookie_sku': 'PB01',
        'quantity': '24',
        'unit_cost': '0.25'
    },
    {
        'cookie_name': 'oatmeal raisin',
        'cookie_recipe_url': 'http://some.okay.me/cookie/raisin.html',
        'cookie_sku': 'EWW01',
        'quantity': '100',
        'unit_cost': '1.00'
    }
]

result = connection.execute(ins, inventory_list)
print_inserted_info(result)



# Creating additional reflections
users = reflection_factory.create_reflect('users')
line_items = reflection_factory.create_reflect('line_items')
orders = reflection_factory.create_reflect('orders')

# Inserting values to reflected table users
customer_list = [
    {
        'username': "cookiemon",
        'email_address': "mon@cookie.com",
        'phone': "111-111-1111",
        'password': "password"
    },
    {
        'username': "cakeeater",
        'email_address': "cakeeater@cake.com",
        'phone': "222-222-2222",
        'password': "password"
    },
    {
        'username': "pieguy",
        'email_address': "guy@pie.com",
        'phone': "333-333-3333",
        'password': "password"
    }
]

users_ins = users.insert()
result = connection.execute(users_ins, customer_list)
display('inserted rows:', result.rowcount)

# Inserting values to reflected line_items
ins = insert(line_items)
order_items = [
    {
        'order_id': 1,
        'cookie_id': 1,
        'quantity': 2,
        'extended_cost': 1.00
    },
    {
        'order_id': 1,
        'cookie_id': 3,
        'quantity': 12,
        'extended_cost': 3.00
    }
]
result = connection.execute(ins, order_items)
display('inserted rows:', result.rowcount)

# Inserting values to reflected orders
ins = insert(orders).values(user_id=1, order_id=1)
result = connection.execute(ins)

