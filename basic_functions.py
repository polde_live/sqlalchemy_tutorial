# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Selecting data from database, using functions in querries.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

# DB should be created - if not run basic_ddl.create_cookies_database

from sqlalchemy import MetaData, select, create_engine, func
from db_utils import ReflectionFactory, print_type, print_sql

# Create connection to database
metadata = MetaData()
engine = create_engine(r'sqlite:///cookies.db')
connection = engine.connect()

reflection_factory = ReflectionFactory(engine, metadata)
cookies = reflection_factory.create_reflect('cookies')

# Count function example
sel_cnt = select([func.count(cookies).label('inventory_count')])
rp = connection.execute(sel_cnt)
display(rp.scalar())

# Sum function example
sel_sum = select([func.sum(cookies.c.unit_cost)])
rp = connection.execute(sel_sum)
display(rp.scalar())

# arthmetic operations
sel_artm = select([cookies.c.cookie_name,
                   cookies.c.quantity,
                   (cookies.c.quantity*2).label('double')])
rp = connection.execute(sel_artm)
display(rp.keys())
display(rp.fetchall())
