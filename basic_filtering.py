# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Selecting data from database, filtering the results
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

# DB should be created - if not run basic_ddl.create_cookies_database

from sqlalchemy import MetaData, select, create_engine, and_
from db_utils import ReflectionFactory, print_type, print_sql

# Create connection to database
metadata = MetaData()
engine = create_engine(r'sqlite:///cookies.db')
connection = engine.connect()

reflection_factory = ReflectionFactory(engine, metadata)
cookies = reflection_factory.create_reflect('cookies')

# basic filter
sel = select([cookies])

sel_filter = sel.where(cookies.c.cookie_name == 'chocolate chip')
rp = connection.execute(sel_filter)
display(rp.fetchall())

# like string filter
sel_like_filter = sel.where(cookies.c.cookie_name.like('%chocolate%'))
rp = connection.execute(sel_like_filter)
display(rp.fetchall())

# chaining filter clauses
sel_chain_filter = sel.where(cookies.c.cookie_name.like('%chocolate%'))
sel_chain_filter = sel_chain_filter.where(cookies.c.quantity <= 12)
rp = connection.execute(sel_chain_filter)
display(rp.fetchall())

# and_ or_ conjunctions
sel_conjunc = sel.where(and_(
    cookies.c.quantity <= 12,
    cookies.c.cookie_name.like('%chocolate%')
    ))
rp = connection.execute(sel_conjunc)
display(rp.fetchall())
