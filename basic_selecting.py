# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Basic selecting data from database.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

# DB should be created - if not run basic_ddl.create_cookies_database

from sqlalchemy import (MetaData, select, desc, create_engine)
from db_utils import ReflectionFactory, print_type, print_sql

# Create connection to database
metadata = MetaData()
engine = create_engine(r'sqlite:///cookies.db')
connection = engine.connect()

reflection_factory = ReflectionFactory(engine, metadata)
cookies = reflection_factory.create_reflect('cookies')

# Basic select *
sel_all = select([cookies])
print_type(sel_all, 'sel object:')
print_sql(sel_all)

# execute and store in ResultProxy
res_prox = connection.execute(sel_all)

# fetchall method clears ResultProxy, returns list of RowProxy objects
results = res_prox.fetchall()
result = results[0]

# Printing  output info
print_type(results)
print_type(result)

display(res_prox.keys())
display(results)

# RowProxy object has keys and store data from single row
uc = result['unit_cost']

# Select * as table reflection method
sel_all = cookies.select()
res_prox = connection.execute(sel_all)

# fetchamy can by used for fetching batches of data, until all rows are returned
iters = 0
while True:
    res = res_prox.fetchmany(2)
    if not res: break
    print('iteration', iters, res)
    iters += 1


# Select columns by name
sel_by_name = select([cookies.c.cookie_id, cookies.c.cookie_name, cookies.c.quantity])
res_prox = connection.execute(sel_by_name)
results = res_prox.fetchall()
display(results)

# Ordering returned output
sel_order = sel_by_name.order_by(desc(cookies.c.cookie_id)).limit(5)
res_prox = connection.execute(sel_order)
display(res_prox.fetchall())