# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Creating lacal database and metadata.
Basic DDL for test tables.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

from datetime import datetime
from sqlalchemy import MetaData
from sqlalchemy import (Table, Column, Integer, Numeric, String, ForeignKey, DateTime, create_engine)

def create_cookies_database():
    """
    Create cookies sqlite database as local file cookies.db.
    If database file already in direcotry, it will not be recreated.
    Function returns sqlalchemy engine object.
    """

    # Connect to database (local sqlite db)
    engine = create_engine(r'sqlite:///cookies.db')
    metadata = MetaData()

    # Basic DDL - creating tables
    cookies = Table('cookies', metadata,
        Column('cookie_id', Integer(), primary_key=True),
        Column('cookie_name', String(50), index=True),
        Column('cookie_recipe_url', String(255)),
        Column('cookie_sku', String(55)),
        Column('quantity', Integer()),
        Column('unit_cost', Numeric(12, 2))
    )

    users = Table('users', metadata,
        Column('user_id', Integer(), primary_key=True),
        Column('username', String(15), nullable=False, unique=True),
        Column('email_address', String(255), nullable=False),
        Column('phone', String(20), nullable=False),
        Column('password', String(25), nullable=False),
        Column('created_on', DateTime(), default=datetime.now),
        Column('updated_on', DateTime(), default=datetime.now, onupdate=datetime.now)
    )

    orders = Table('orders', metadata,
        Column('order_id', Integer(), primary_key=True),
        Column('user_id', ForeignKey('users.user_id')),
    )

    line_items = Table('line_items', metadata,
        Column('line_items_id', Integer(), primary_key=True),
        Column('order_id', ForeignKey('orders.order_id')),
        Column('cookie_id', ForeignKey('cookies.cookie_id')),
        Column('quantity', Integer()),
        Column('extended_cost', Numeric(12, 2))
    )

    # Create all tables stored in metadata
    metadata.create_all(engine)
    return engine

def print_db_info(engine):
    # List all crated objects
    print(engine.table_names())

def main():
    """Main function for basic_ddl module.
    If run as main, recreate database from scratch.
    """
    cookies_db = create_cookies_database()
    print_db_info(cookies_db)

if __name__ == "__main__":
    exit(int(main() or 0))