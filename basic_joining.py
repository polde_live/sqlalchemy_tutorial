# -*- coding: utf-8 -*-
"""
Basic sqlalchemy ORM functionalities.
Selecting data from database, joining multiple tables.
Source code avilable at:
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""
from sqlalchemy import MetaData, select, create_engine, func
from db_utils import ReflectionFactory, print_type, print_sql

# Create connection to database
metadata = MetaData()
engine = create_engine(r'sqlite:///cookies.db')
connection = engine.connect()

reflection_factory = ReflectionFactory(engine, metadata)
cookies = reflection_factory.create_reflect('cookies')
orders = reflection_factory.create_reflect('orders')
# joining on table keys
columns = [users.c.username, orders.c.order_id]
all_orders = select(columns)
all_orders = all_orders.select_from(users.outerjoin(orders))
print_sql(all_orders)

result = connection.execute(all_orders).fetchall()
print(result)

# joining on columns
sel_join = select([users.c.username, func.count(orders.c.order_id).label('orders_count')])
sel_join = sel_join.select_from(users.outerjoin(orders, users.c.user_id == orders.c.user_id))
sel_join = sel_join.group_by(users.c.username)
rp = connection.execute(sel_join)

print(rp.keys())
print(rp.fetchall())

