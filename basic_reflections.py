# -*- coding: utf-8 -*-
"""
Basic sqlalchemy core functionalities.
Reflections of chinook database.
https://github.com/oreillymedia/essential-sqlalchemy-2e
"""

from sqlalchemy import (create_engine, MetaData, Table)

engine = create_engine('sqlite:///chinook\Chinook_Sqlite.sqlite')
meta = MetaData()

# Create reflected table
artist = Table('Artist', meta, autoload=True, autoload_with=engine)
album = Table('Album', meta, autoload=True, autoload_with=engine)

# Display reflected table columns
print(artist.columns.keys())
print(album.columns.keys())
# Select data from reflected table
from sqlalchemy import select

sel = select([artist]).limit(10)
engine.execute(sel).fetchall()

# Add missing FK constraint
# Artist was reflected before Album, FK is missing on Artist table
from sqlalchemy import ForeignKeyConstraint
album.append_constraint(
    ForeignKeyConstraint(['ArtistId'], ['artist.ArtistId'])
    )

# Joining tables on added FK
str(artist.join(album))

sel = select([artist, album]).select_from(artist.join(album))
sel = sel.limit(10)
rp = engine.execute(sel)
rp.keys()
rp.fetchall()

# Reflecting entire database
meta.reflect(bind=engine)
meta.tables.keys()

playlist = meta.tables['Playlist']
sel = select([playlist]).limit(10)
engine.execute(sel).fetchall()

